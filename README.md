# MLOps_project

BEAUREPAIRE Margot

BRIAND Julien

LAUR Simon


https://tinyurl.com/mlops-paper

## Application

1. root : https://mlops-beaurepaire-briand-laur.herokuapp.com/

2. documentation : https://mlops-beaurepaire-briand-laur.herokuapp.com/docs

3. intent : https://mlops-beaurepaire-briand-laur.herokuapp.com/api/intent/<sentence>

4. supported languages : https://mlops-beaurepaire-briand-laur.herokuapp.com/api/intent-supported-languages

5. health : https://mlops-beaurepaire-briand-laur.herokuapp.com/health

## Benchmarck

1. «Vos services sont vachement gourmand coté ML.
   Tu peux me fournir la taille de ton image docker et la limite de RAM à définir sur les environnements stp ?» - taille image docker : 665,98 Mb - limite de RAM : 277,5 Mb

2. «Le modèle est viable jusqu’à quel trafic en production ? On souhaite avoir un P99 < 200ms»
   Le modèle est viable jusqu’à un trafic en production de 58 utilisateurs.
   En effet, on remarque qu'on a un P95<190ms lorsqu’il y a 58 utilisateurs sur le site (cf figure 1).
   On considère alors que P99<190ms pour 58 utilisaturs.

![](190ms.png)

Figure 1 : Capture d’écran montrant le temps médian pour 58 utilisateurs

Or à partir de 60 utilisateurs, on a P95<200ms (cf figure 2). On considère alors que P99<200ms pour 60 utilisateurs.
Bien que cela fluctue entre 190 et 200 par la suite, il est préférable de ne pas prendre de risque et ainsi, définir que le modèle est viable jusqu’à 58 utilisateurs.

![](200ms.png)
Figure 2 : Capture d’écran montrant le temps médian pour 60 utilisateurs

3. «On veut enrichir de la donnée dans la stack data avec ton modèle mais ça rame fort, tu as des idées pour améliorer les perfs ?»
   Afin d'améliorer les performances du modèle, nous pourrions mettre dans des batchs les différentes données reçues par le modèle.
   Ceci permettrait de traiter plusieurs données à chaque inférence du modèle.
   Une autre solution pourrait être de réduire le temps d'inférence du modèle en place.
