import main
from json import dumps
from fastapi.responses import JSONResponse
import spacy

nlp = spacy.load("./models")

def test_supported_languages():
    assert main.supported_languages() == dumps(["fr-FR"])

def test_health_check_endpoint():
    assert main.health_check_endpoint().status_code == JSONResponse(content='', status_code=200).status_code

def test_intent_inference():
    sentence = "test"
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    assert main.intent_inference("test") == dumps(inference.cats)