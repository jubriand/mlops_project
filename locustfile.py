from locust import HttpUser, task, between

class User(HttpUser):
    @task(3)
    def sentence(self):
        sentence="/api/intent/bonjour"
        self.client.get(sentence)

    @task(5)
    def language(self):
        self.client.get("/api/intent-supported-languages")
    
    @task
    def health(self):
        self.client.get("/health")

