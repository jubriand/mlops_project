# syntax=docker/dockerfile

FROM python:3.8-slim-buster


COPY . .

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git && \
    apt-get install -y ruby-dev && \
    apt-get install -y curl

RUN pip install -r requirements.txt
# RUN pip install fastapi
# RUN pip install uvicorn
RUN pip install dpl
RUN pip install heroku
RUN pip install poetry

RUN poetry install --no-dev

EXPOSE 8080

CMD [ "poetry", "run", "python", "main.py"] 

#image sha256:8a45a4bbd77c8c3dd2f41286a8552400f59b3dc598aa00a1c732add7759ef420