import spacy
import logging
from json import dumps
from fastapi import FastAPI
from fastapi.responses import JSONResponse
import uvicorn
import os

logging.info("Loading model..")
nlp = spacy.load("./models")

app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}

@app.get("/api/intent/{sentence}")
def intent_inference(sentence) :
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return dumps(inference.cats)


@app.get("/api/intent-supported-languages")
def supported_languages():
    return dumps(["fr-FR"])

@app.get("/health")
def health_check_endpoint():
    return JSONResponse(content=None, status_code=200)

if __name__ == "__main__":
    uvicorn.run("__main__:app", host="0.0.0.0", port=int(os.environ.get('PORT', 8080)), reload=True, workers=2)